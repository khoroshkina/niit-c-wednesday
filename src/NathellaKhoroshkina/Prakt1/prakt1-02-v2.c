/* �������� ���������, ������� ����������� ������� ����� � ������� ��:��:��,
� ����� ������� ����������� � ����������� �� ���������� ("������ ����", "������ ����" � �.�.)*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main ()
{

	int hr, min, sec, ch;
	
    while (1)
    {
        printf (" Please enter your time (hr:min:sec)\n");
        if ((scanf ("%d:%d:%d", &hr, &min, &sec) == 3) && (min >=00 && min <= 60) && (sec >=00 && sec <=60))
        {
            if (hr>=00 && hr<=06)
            {
                printf ("Good Night!\n");
                break;
            }
            else if (hr>06 && hr<=12)
            {
                printf ("Good Morning!\n");
                break;
            }
            else if (hr>12 && hr<=18)
            {
                printf ("Good Day!\n");
                break;
            }
            else if (hr >18 && hr<=24)
            {
                printf ("Good Evening!\n");
                 break;
            }
        }
        
        else
            printf("You entered the incorrect time. Re-enter please!\n");
        do
                  ch = getchar();
        while (ch != '\n'&& ch != EOF);

    }
            
    printf("Your entered time is %02d:%02d:%02d.\n", hr, min, sec);
    return 0;
}