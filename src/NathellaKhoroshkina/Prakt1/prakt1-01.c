/*
�������� ���������, ������� ����������� � ������������ ���, ���� � ���, 
� ����� ����������� ����������� ����� � ����, ������� ������������
� ���������� ��������� (��������, ����������, �����)
���=���/(����(�)*����(�))
��� ������: 19<=������<=24
��� ������: 18<=������<=25
*/
#include <stdio.h>
#include <math.h>
#include <conio.h>

int main ()
{
	char gender;
	double height;
	double weight;
	double index;

	const double f1=19.0, f2=24.0, m1=18.0, m2=25.0;

	printf ("Please specify your gender (m or f):\n");
	scanf ("%c",&gender);

	if (gender!='f' && gender!='F' && gender!='m' && gender!='M')
		printf ("Error!\n");
	else
	{
		printf ("Please enter your height (for example, 1.75):\n");
		scanf ("%lf",&height);
		if (height<0)
			printf ("Error!\n");
		else
		{
			printf ("Please enter your weight (for example, 84)\n");
			scanf ("%lf",&weight);
			if (weight<0)
				printf ("Error!\n");
			else
			{
				index=weight/(height*height/10000.0);

				switch (gender)
				{
				case 'f': 
				case 'F':
					{
						if (index>=f1 && index<=f2)
							printf ("Your weight is within normal limits.\n");
						else if (index<f1)
							printf ("You have a very low body weight. You must see your doctor.\n");
						else
							printf ("Your weight exceeds the norm. You must see your doctor.\n");
						break;
					}
				case 'm': 
				case 'M':
					{
						if (index>=m1 && index<=m2)
							printf ("Your weight is within normal limits.\n");
						else if (index<m1)
							printf ("You have a very low body weight. You must see your doctor.\n");
						else
							printf ("Your weight exceeds the norm. You must see your doctor.\n");
						break;
					}
				}
			}
		}
	}
    getch();
    return 0;
}