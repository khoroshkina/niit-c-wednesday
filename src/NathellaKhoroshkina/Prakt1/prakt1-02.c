/* �������� ���������, ������� ����������� ������� ����� � ������� ��:��:��,
� ����� ������� ����������� � ����������� �� ���������� ("������ ����", "������ ����" � �.�.)*/


#include <stdio.h>
#include <math.h>

int main ()
{
    int cc, mm=0, ss=0;
    printf ("Enter time? (CC:MM:SS)\n");
    scanf ("%d:%d:%d", &cc, &mm, &ss);

    if (cc<0 || cc>24 || mm<0 || mm>60 || ss<0 || ss>60)
    {
        printf ("Error. Uncorrect time.\n");
    }
    else
    {
        if (cc>=0 && cc<6)
            printf ("Good Night!\n");
        else if (cc>=6 && cc<12)
            printf ("Good Morning!\n");
        else if (cc>=12 && cc<18)
            printf ("Good Afternoon!\n");
        else if (cc=24 && (mm!=0 || ss!=0))
            printf ("Error\n");
        else 
            printf ("Good Evening!\n");
        printf ("Time: %d:%d:%d\n", cc, mm, ss);
        return 0;
    }    
}


