/*
�������� ���������, ��������� �� ����� ����������� �� ���������.
���������:
����������� ������ ��������� ���:
    *
   ***
  *****
���������� ����� �������� ������������� � ����������
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main ()
{
	int dig,i,j,k;

	printf ("Enter digite\n");
	scanf ("%d", &dig);
	
	for (i=1; i<=dig; i++)
	{
		for (j=1; j<=dig-i; j++)
			printf("%c", ' ');
		for (j=1; j<=2*i-1; j++)
			printf("%c", '*');
		printf ("\n");
	}
	
	return 0;
}